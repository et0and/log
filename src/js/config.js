const calendars = ['aequirys', 'desamber', 'gregorian'];
const secpro = ['sector', 'sec', 'project', 'pro'];
const timeformats = ['24', '12', 'decimal'];
const statformats = ['decimal', 'human'];
module.exports = class Config {
  /**
   * Construct config
   * @param {Object} attr
   * @param {string} attr.ac - Accent colour
   * @param {string} attr.bg - Background colour
   * @param {number} attr.ca - Calendar
   * @param {string} attr.cm - Colour mode
   * @param {string} attr.fg - Foreground colour
   * @param {number} attr.st - Stat format
   * @param {number} attr.tm - Time format
   * @param {number} attr.vw - View
   */
  constructor (attr) { Object.assign(this, attr); }

  /**
   * Set accent
   * @param {string} a - Accent
   */
  setAccent (a) {
    this.ac = a;
    Update.config();
  }

  /**
   * Set background colour
   * @param {string} c
   */
  setBackgroundColour (c) {
    this.bg = c;
    Update.config();
  }

  /**
   * Set calendar system
   * @param {string} c
   */
  setCalendar (c) {
    if (calendars.indexOf(c) < 0) return;
    cDisplay = {};

    let n = 0;
    switch (c) {
      case 'aequirys':
      case 'desamber':
        n = 1;
        break;
      default:
        break;
    }

    this.ca = n;
    Update.config();
  }

  /**
   * Set colour mode
   * @param {string} m - Sector, project, or none
   */
  setColourMode (m) {
    if (secpro.indexOf(m) < 0 && m !== 'none') return;
    let n;
    switch (m) {
      case 'sector': case 'sec': n = 'sc'; break;
      case 'project': case 'pro': n = 'pc'; break;
      case 'none': n = 'none'; break;
      default: break;
    }

    this.cm = n;
    Update.config();
  }

  /**
   * Set foreground colour (text colour)
   * @param {string} c
   */
  setForegroundColour (c) {
    this.fg = c;
    Update.config();
  }

  /**
   * Set stat display format
   * @param {string} f - Decimal or human
   */
  setStatFormat (f) {
    if (statformats.indexOf(f) < 0) return;
    this.st = +!(f === 'decimal');
    Update.config();
  }

  /**
   * Set time system
   * @param {string} f - 24, 12, or decimal
   */
  setTimeFormat (f) {
    if (timeformats.indexOf(f) < 0) return;

    let n = 0;
    switch (f) {
      case '24': n = 1; break;
      case 'decimal': n = 2; break;
      default: break;
    }

    this.tm = n;
    Update.config();
  }

  /**
   * Set view to n days
   * @param {number} n
   */
  setView (n) {
    if (n < 0) return;
    this.vw = n;
    Update.config();
  }
};
