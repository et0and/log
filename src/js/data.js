/**
 * Calc sum
 * @param {Array=} v - Values
 * @return {number}
 */
function sum (v = []) {
  const l = v.length;
  if (l === 0) return 0;
  let x = 0;
  for (let i = 0; i < l; i++) x += v[i];
  return x;
}

/**
 * Calc average
 * @param {Array=} v - Values
 * @return {number}
 */
function avg (v = []) {
  const l = v.length;
  return l === 0 ? 0 : sum(v) / l;
}

/**
 * Calc max
 * @param {Array=} v - Values
 * @return {number}
 */
function max (v = []) {
  let l = v.length;
  if (l === 0) return 0;
  let m = 0;
  while (l--) {
    if (v[l] > m) m = v[l];
  }
  return m;
};

/**
 * Calc min
 * @param {Array=} v - Values
 * @return {number}
 */
function min (v = []) {
  let l = v.length;
  if (l === 0) return 0;
  let m = Infinity;
  while (l--) {
    if (v[l] < m) m = v[l];
  }
  return m;
};

/**
 * Calc ceiling
 * @param {number=} n
 * @return {number} Ceiling
 */
function ceil (n) {
  return n % 1 === 0 ? n : ~~n + 1
}

/**
 * Check if value is a number
 * @param n - number?
 * @return {boolean} Is Number?
 */
function isNumber (n) {
  return !isNaN(parseFloat(n)) && !isNaN(n - 0)
}

/**
 * Add leading zeroes
 * @return {string}
 */
function pad (n) {
  return `0${n}`.substr(-2);
}

/**
 * Parse logs
 * @param {Array=} logs
 * @return {LogSet}
 */
function parse (logs = LOG.entries) {
  const l = logs.length;
  if (l === 0) return new LogSet(logs);
  const diffDay = (s, e) => toDate(s) !== toDate(e);
  const p = [];

  function split (a, b, {id, x, c, t, d}) {
    const dx = new Date(a);
    const dy = new Date(b);

    dx.setHours(23, 59, 59);
    dy.setHours(0, 0, 0);

    p[p.length] = new Entry({id, s: a, e: dx, ci: x, c, t, d});
    p[p.length] = new Entry({id, s: dy, e: b, ci: x, c, t, d});
  }

  for (let id = 0; id < l; id++) {
    const {s, e, x, c, t, d} = logs[id];
    const a = new Date(s);
    const b = e === undefined ? undefined : new Date(e);

    if (e !== undefined && diffDay(a, b)) {
      split(a, b, {id, c, t, d});
    } else {
      p[p.length] = new Entry({id, s: a, e: b, ci: x, c, t, d});
    }
  }

  return new LogSet(p);
}

/**
 * Calc range
 * @param {Array=} v - Values
 * @return {number} Range
 */
function range (v = []) {
  if (v.length === 0) return 0;
  const s = v.sort();
  return s[s.length - 1] - s[0];
}

/**
 * Calc standard deviation
 * @param {Array=} v - Values
 * @return {number}
 */
function sd (v = []) {
  const l = v.length;
  if (l === 0) return 0;
  const x = avg(v);
  let y = 0;
  for (let i = 0; i < l; i++) {
    y += (v[i] - x) ** 2;
  }
  return (y / (l - 1)) ** .5;
}

/**
 * Display as stat
 * @param {number=} format - Stat format
 * @return {string} Stat
 */
function toStat (n, format = LOG.config.st) {
  switch (format) {
    case 0: return n.toFixed(2);
    case 1: {
      const m = n % 1;
      const tail = +(m * 60).toFixed(0);
      return `${n - m}:${pad(tail)}`;
    }
    default: return n;
  }
}

/**
 * Calc trend
 * @param {number} x
 * @param {number} y
 * @return {string} Trend
 */
function trend (x, y) {
  const p = (x - y) / y * 100;
  const s = p < 0 ? '' : '+';
  return `${s}${p.toFixed(2)}%`;
}
