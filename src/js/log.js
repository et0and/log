/**
 * Log
 * A single-task time-tracker
 *
 * @author Josh Avanier
 * @license MIT
 */

/**
 * Create Object
 * @param {string} el
 * @param {Object} params
 * @return {Object}
 */
function ø (e, params) {
  return Object.assign(document.createElement(e), params);
}

const CLI = window.require('./js/cli');
let Glossary = {};
let Palette = {};
let Session = {};

const LOG = {
  CLOCK: {},
  config: {},
  entries: [],
  cache: {
    sor: [],
    dur: [],
    pkh: [],
    pkd: [],
    pro: [],
    sec: [],
  },

  /**
   * Get log status
   * @return {boolean} Status
   */
  status () {
    return Session.count === 0 ? false : !Session.last.end;
  },

  /**
   * Display session time
   */
  timer () {
    if (LOG.status() === false) return;
    const l = +Session.last.start;
    let d = +new Date();
    let h = 0;
    let m = 0;
    let s = 0;

    LOG.CLOCK = setInterval(() => {
      d += 1E3;

      s = ~~((d - l) / 1E3);
      m = ~~(s / 60);
      h = ~~(m / 60);

      h = pad(h %= 24);
      m = pad(m %= 60);
      s = pad(s %= 60);

      UI.timerEl.innerHTML = `${h}:${m}:${s}`;
    }, 1E3);
  },

  /**
   * Play sound
   * @param {string} sound
   */
  playSound (sound) {
    new Audio(`${__dirname}/media/${sound}.mp3`).play();
  },

  setEditFormValues (id) {
    const {s, e, c, t, d} = LOG.entries[id];

    function getValue (x) {
      const xy = x.getFullYear();
      const xm = pad((x.getMonth() + 1));
      const xd = pad(x.getDate());
      const xh = pad(x.getHours());
      const xn = pad(x.getMinutes());
      const xs = pad(x.getSeconds());
      return `${xy}-${xm}-${xd}T${xh}:${xn}:${xs}`;
    }

    editID.innerHTML = id + 1;
    editEntryID.value = id;
    editSector.value = c;
    editProject.value = t;
    editDesc.value = d;
    editStart.value = getValue(new Date(s));

    if (e !== undefined && typeof e === 'number') {
      editEnd.value = getValue(new Date(e));
    }
  },

  /**
   * Summon Edit modal
   * @param {number} id - Entry ID
   */
  edit (id) {
    editEnd.value = '';
    LOG.setEditFormValues(id);
    UI.modalMode = true;
    document.getElementById('editModal').showModal();
  },

  /**
   * Summon Delete modal
   * @param {string} i - Command input
   */
  confirmDelete (input) {
    delList.innerHTML = '';

    const words = input.split(' ').slice(1);
    const mode = words[0];
    const key = words[1];
    let confirmation = '';

    function count (prop) {
      const {entries} = LOG;
      const l = entries.length;
      let c = 0;
      for (let i = 0; i < l; i++) {
        if (entries[i][prop] === key) c++;
      }
      return c;
    }

    if (mode === 'project') {
      confirmation = `Are you sure you want to delete the ${key} project? ${count('t')} entries will be deleted. This can't be undone.`;
    } else if (mode === 'sector') {
      confirmation = `Are you sure you want to delete the ${key} sector? ${count('c')} entries will be deleted. This can't be undone.`;
    } else {
      const aui = words.filter((v, i, self) => self.indexOf(v) === i).sort();
      const span = ø('span', {className: 'mr3 o7'});

      confirmation = `Are you sure you want to delete the following ${aui.length > 1 ? `${aui.length} entries` : 'entry'}? This can't be undone.`;

      aui.forEach((i) => {
        const {s, e, c, t, d} = LOG.entries[+i - 1];
        const ss = stamp(new Date(s));
        const se = stamp(new Date(e));
        const li = ø('li', {className: 'f6 lhc pb3 mb3'});
        const id = Object.assign(span.cloneNode(), {innerHTML: i});
        const tm = Object.assign(span.cloneNode(), {
          innerHTML: `${ss} &ndash; ${se}`
        });
        const sc = Object.assign(span.cloneNode(), {innerHTML: c});
        const pr = Object.assign(document.createElement('span'), {
          className: 'o7', innerHTML: t
        });
        const dc = Object.assign(document.createElement('p'), {
          className: 'f4 lhc', innerHTML: d
        });

        li.append(id);
        li.append(tm);
        li.append(sc);
        li.append(pr);
        li.append(dc);
        delList.append(li);
      });
    }

    delConfirm.setAttribute('onclick', `LOG.deleteIt('${input}')`);
    delMessage.innerHTML = confirmation;
    delModal.showModal();
  },

  /**
   * Hacky solution
   */
  deleteIt (i) {
    Command.deleteEntry(i);
  },

  /**
   * Update entry
   * @param {number} id - Entry ID
   */
  update (id, {s, e, c, t, d}) {
    Object.assign(LOG.entries[id], {s, e, c, t, d});
    data.set('log', LOG.entries);
    editModal.close();
    UI.modalMode = false;
    LOG.refresh();
  },

  /**
   * View Details
   * @param {number} mode - Sector (0) or project (1)
   * @param {string} key
   */
  viewDetails (mode, key) {
    if (mode < 0 || mode > 1) return;
    const d = document.getElementById(!mode ? 'SSC' : 'PSC');
    d.innerHTML = '';
    d.append(UI.details.detail.build(mode, key));
  },

  reset () {
    clearInterval(LOG.CLOCK);
    document.getElementById('ui').innerHTML = '';
    console.log('Reset');
  },

  generateSessionCache () {
    if (LOG.entries.length === 0) return;
    Object.assign(LOG.cache, {
      pro: Session.listProjects(),
      sec: Session.listSectors(),
      pkh: Session.peakHours(),
      pkd: Session.peakDays()
    });
  },

  load () {
    function ä (obj) {
      return Object.assign(obj, {
        backgroundColor: LOG.config.bg,
        color: LOG.config.fg
      });
    }

    LOG.generateSessionCache();

    ä(document.body.style);
    ä(ui.style);

    UI.build();
    UI.util.setTimeLabel();
    UI.util.setDayLabel();

    if (LOG.entries.length === 0) Nav.index = 5;
    Nav.tab(Nav.menu[Nav.index]);
  },

  refresh () {
    LOG.reset();
    LOG.load();
  },

  init (data) {
    if (data === undefined) {
      console.error('Missing data store');
      return;
    }

    try {
      const ent = data.get('log');
      Session = parse(ent);
      Object.assign(LOG, {
        config: new Config(data.get('config')),
        entries: ent
      });
      Object.assign(Palette, {
        pp: data.get('pp'),
        sp: data.get('sp')
      });
    } catch (e) {
      console.error(e);
      notify('Something went wrong.');
      return;
    }

    Glossary = new Lexicon({
      path: `${__dirname}/lexicon/en.json`
    }).data;

    CLI.installHistory();
    LOG.load();

    document.onkeydown = (e) => {
      if (UI.modalMode) return;

      function focus () {
        UI.commanderEl.style.display = 'block';
        UI.commanderInput.focus();
      }

      if (e.which >= 65 && e.which <= 90) {
        focus();
        return;
      }

      if (e.which >= 48 && e.which <= 54 && (e.ctrlKey || e.metaKey)) {
        Nav.index = e.which - 49;
        Nav.tab(Nav.menu[Nav.index]);
        return;
      }

      const l = CLI.history.length;

      switch (e.which) {
        case 9: // Tab
          e.preventDefault();
          Nav.next();
          break;
        case 27: // Escape
          UI.commanderEl.style.display = 'none';
          UI.commanderInput.value = '';
          UI.comIndex = 0;
          break;
        case 38: // Up
          focus();
          UI.comIndex++;
          if (UI.comIndex > l) UI.comIndex = 1;
          UI.commanderInput.value = CLI.history[l - UI.comIndex];
          break;
        case 40: // Down
          focus();
          UI.comIndex--;
          if (UI.comIndex < 1) UI.comIndex = 1;
          UI.commanderInput.value = CLI.history[l - UI.comIndex];
          break;
        default:
          break;
      }
    };

    document.addEventListener('click', ({target}) => {
      if (target === entryModal) entryModal.close();
    });
  }
};
