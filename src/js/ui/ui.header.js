module.exports = {

  /**
   * Build Header
   * @return {Object}
   */
  build () {
    const header = ø('header', {className: 'mb2 f6 lhc'});
    header.append(this.nav());
    header.append(this.clock());
    return header;
  },

  /**
   * Build Navigation
   * @return {Object} Navigation menu
   */
  nav ({menu} = Nav, {tabs} = Glossary) {
    const fr = document.createDocumentFragment();

    function ä (id, innerHTML, opacity = 'o5') {
      fr.append(ø('button', {
        className: `pv1 tab on bg-cl ${opacity} mr3`,
        onclick: () => Nav.tab(id),
        id: `b-${id}`,
        innerHTML
      }));
    }

    ä(menu[0], tabs.overview, 'of');
    ä(menu[1], tabs.details);
    ä(menu[2], tabs.entries);
    ä(menu[3], tabs.journal);
    ä(menu[4], tabs.guide);

    return fr;
  },

  /**
   * Build Clock
   * @return {Object} Clock
   */
  clock () {
    UI.timerEl = ø('span', {
      className: 'rf f5 di tnum',
      innerHTML: '00:00:00'
    });

    LOG.timer();
    return UI.timerEl;
  }
};
