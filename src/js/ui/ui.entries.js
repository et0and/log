module.exports = {

  /**
   * Build Entries
   * @return {Object}
   */
  build () {
    const f = document.createDocumentFragment();
    f.append(this.table());
    f.append(this.modal());
    return f;
  },

  /**
   * Build Entries table
   * @return {Object}
   */
  table () {
    function ä (e, className, innerHTML = '') {
      return ø(e, {className, innerHTML});
    }

    const f = document.createDocumentFragment();
    const T = ä('table', 'wf bn f6');
    const h = ä('thead', 'al');
    const b = ä('tbody', 'nodrag');

    const n = [
      Glossary.date,
      Glossary.time,
      'CI',
      Glossary.sec.singular,
      Glossary.pro.singular
    ];

    const el = LOG.entries.length;
    const arr = LOG.entries.slice(el - 100).reverse();

    function idAttr (id, i) {
      return {
        onclick: () => LOG.edit(id),
        className: 'pl0 c-pt hover',
        innerHTML: el - i,
      };
    }

    function dateAttr (s, sd) {
      return {
        onclick: () => Nav.toJournal(s),
        className: 'c-pt hover',
        innerHTML: display(sd)
      };
    }

    function ctAttr (m, t) {
      return {
        onclick: () => Nav.toDetail(m, t),
        className: 'c-pt hover',
        innerHTML: t
      };
    }

    for (let i = 0, l = arr.length; i < l; i++) {
      const {s, e, x, c, t, d} = arr[i];
      const sd = new Date(s);
      const ed = new Date(e);
      const st = stamp(sd);
      const id = el - i - 1;
      const r = Object.assign(
        document.createElement('tr'), {id: `r${id}`}
      );
      const time = document.createElement('td');

      time.innerHTML = e === undefined
        ? `${st} –`
        : `${st} – ${stamp(new Date(e))} (${toStat(duration(sd, ed))})`

      r.appendChild(ø('td', idAttr(id, i)));
      r.appendChild(ø('td', dateAttr(s, sd)));
      r.appendChild(time);
      r.appendChild(ø('td', {innerHTML: x}));
      r.appendChild(ø('td', ctAttr(0, c)));
      r.appendChild(ø('td', ctAttr(1, t)));
      r.appendChild(ä('td', 'pr0', d));
      b.appendChild(r);
    }

    T.append(h);
    h.append(ä('th', 'pl0 fwn', Glossary.id));
    for (let i = 0, l = n.length; i < l; i++) {
      h.append(ä('th', 'fwn', n[i]));
    }
    h.append(ä('th', 'pr0 fwn', Glossary.desc));
    T.append(b);
    f.append(T);

    return f;
  },

  /**
   * Build Entries modal
   * @param {Object} config
   * @param {string} config.bg - Background
   * @param {string} config.fg - Foreground
   * @return {Object}
   */
  modal ({bg, fg} = LOG.config) {
    const m = Object.assign(
      document.createElement('dialog'), {
        id: 'editModal',
        className: 'p4 cn bn h6',
        onkeydown: (e) => {
          if (e.key === 'Escape') UI.modalMode = false;
        }
      }
    );

    const f = Object.assign(
      document.createElement('form'), {
        onsubmit: () => false,
        className: 'nodrag',
        id: 'editForm'
      }
    );

    const i = Object.assign(
      document.createElement('input'), {
        className: 'db wf p2 mb3 bn'
      }
    );

    Object.assign(m.style, {backgroundColor: bg, color: fg});

    document.addEventListener('click', ({target}) => {
      if (target === m) {
        UI.modalMode = false;
        m.close();
      }
    });

    f.addEventListener('submit', () => {
      const e = editEnd.value === '' ? '' : new Date(editEnd.value);

      LOG.update(editEntryID.value, {
        s: +(new Date(editStart.value)),
        e: e === '' ? undefined : +e,
        c: editSector.value,
        t: editProject.value,
        d: editDesc.value
      });

      UI.modalMode = false;
    });

    m.append(ø('p', {
      id: 'editID',
      className: 'mb4 f6 lhc'
    }));

    m.append(f);

    f.append(ø('input', {
      id: 'editEntryID',
      type: 'hidden'
    }));

    f.append(Object.assign(i.cloneNode(), {
      id: 'editSector',
      type: 'text',
      placeholder: 'Sector'
    }));

    f.append(Object.assign(i.cloneNode(), {
      id: 'editProject',
      type: 'text',
      placeholder: 'Project'
    }));

    f.append(ø('textarea', {
      id: 'editDesc',
      className: 'db wf p2 mb3 bn',
      rows: '3',
      placeholder: 'Description (optional)'
    }));

    f.append(Object.assign(i.cloneNode(), {
      id: 'editStart',
      type: 'datetime-local',
      step: '1'
    }));

    f.append(Object.assign(i.cloneNode(), {
      id: 'editEnd',
      type: 'datetime-local',
      step: '1'
    }));

    f.append(ø('input', {
      id: 'editUpdate',
      className: 'dib p2 mr2 br1 bn',
      type: 'submit',
      value: 'Update'
    }));

    f.append(ø('input', {
      id: 'editCancel',
      className: 'dib p2 br1 bn',
      type: 'button',
      value: 'Cancel',
      onclick: () => {
        UI.modalMode = false;
        m.close();
      }
    }));

    return m;
  }
};
