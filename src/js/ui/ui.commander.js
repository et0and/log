/**
 * Build Commander
 * @return {Object} Commander
 */
module.exports = function Commander () {
  const commander = Object.assign(
    document.createElement('form'), {
      action: '#',
      className: 'dn psf b0 l0 wf f6 z9',
      onsubmit: () => false,
    }
  );

  const input = Object.assign(
    document.createElement('input'), {
      autofocus: 'autofocus',
      className: 'wf bg-0 blanc on bn p3',
      placeholder: Glossary.console,
      type: 'text',
    }
  );

  commander.addEventListener('submit', () => {
    const {history} = CLI;
    const v = input.value;
    LOG.comIndex = 0;

    if (v !== '') {
      const l = history.length;
      if (v !== history[l - 1]) history[l] = v;
      if (l >= 100) history.shift();
      localStorage.setItem('histoire', JSON.stringify(history));
      CLI.parse(v);
    }

    commander.style.display = 'none';
    input.value = '';
  });

  UI.commanderEl = commander;
  UI.commanderInput = input;
  commander.append(input);
  return commander;
};
