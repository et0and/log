const Aequirys = window.require('aequirys');

// Memoization caches
const cDisplay = {};
const cStamp = {};

/**
 * Add days to date
 * @param {number=} i - Increment
 * @return {Date}
 */
function addDays (date, i = 1) {
  const d = new Date(date.valueOf());
  d.setDate(d.getDate() + i);
  return d;
}

/**
 * Calculate time ago
 * @return {string} Time ago
 */
function ago (d) {
  const m = ~~((new Date() - +d) / 6E4);
  if (m === 0) return 'less than a minute ago';
  if (m === 1) return 'a minute ago';
  if (m < 59) return `${m} minutes ago`;
  if (m < 120) return 'an hour ago';
  if (m < 1440) return `${~~(m / 60)} hours ago`;
  if (m < 2880) return 'yesterday';
  if (m < 86400) return `${~~(m / 1440)} days ago`;
  if (m < 1051199) return `${~~(m / 43200)} months ago`;
  return `over ${~~(m / 525960)} years ago`;
}

/**
 * Format date
 * @param {number=} ca - Calendar format
 * @return {string} Formatted date
 */
function formatDate (date, {ca} = LOG.config) {
  switch (ca) {
    case 1: return Aequirys.display(Aequirys.convert(date));
    default: {
      const y = pad(date.getFullYear());
      const d = pad(date.getDate());
      const m = Glossary.months[date.getMonth()][0].toUpperCase();
      return `${d}${m}${y}`;
    }
  }
}

/**
 * Display date
 * @return {string} Formatted date
 */
function display (d) {
  const x = new Date(d).setHours(0, 0, 0, 0);
  return x in cDisplay ? cDisplay[x] : (cDisplay[x] = formatDate(d));
}

/**
 * Display 12h time
 * @return {string} 12h time
 */
function to12H (d) {
  let h = d.getHours();
  const X = h >= 12 ? 'PM' : 'AM';
  const H = pad((h %= 12 ? h : 12));
  const M = pad(d.getMinutes());
  return `${H}:${M} ${X}`;
}

/**
 * Display 24h time
 * @return {string} 24h time
 */
function to24H (d) {
  const h = pad(d.getHours());
  const m = pad(d.getMinutes());
  return `${h}:${m}`;
}

/**
 * Convert to decimal time
 * @param {Date} d
 * @return {string} Decimal beat
 */
function toDec (date) {
  const d = new Date(date);
  const b = new Date(d).setHours(0, 0, 0);
  const v = (d - b) / 864E5;
  const t = v.toFixed(6).substr(2, 6);
  return t.substr(0, 3);
}

/**
 * Format time
 * @param {number=} f - Time format
 * @return {string} Formatted time
 */
function formatTime (d, f = LOG.config.tm) {
  switch (f) {
    case 0: return to12H(d);
    case 1: return to24H(d);
    default: return toDec(d);
  }
}

/**
 * Display timestamp
 * @return {string} Timestamp
 */
function stamp (d) {
  const x = `${d.getHours()}${d.getMinutes()}`;
  return x in cStamp ? cStamp[x] : (cStamp[x] = formatTime(d));
}

/**
 * Convert to date ID
 * @return {string} YYYYMMDD
 */
function toDate (date) {
  const y = date.getFullYear();
  const m = pad(date.getMonth());
  const d = pad(date.getDate());
  return `${y}${m}${d}`;
}

/**
 * Convert hexadecimal to decimal
 * @param {string} h
 * @return {number} Decimal
 */
function convertBase36 (h) {
  return parseInt(h, 36);
}

/**
 * Calculate duration
 * @param {Date} s - Start
 * @param {Date} e - End
 * @return {number} Duration (1 = 1h)
 */
function duration (s, e) {
  return e === undefined ? 0 : (+e - +s) / 36E5;
}

/**
 * List dates
 * @param {Date}  s - Start
 * @param {Date=} e - End
 * @return {Array} List
 */
function listDates (s, e = new Date()) {
  const l = [];

  let n = new Date(s);
  n.setHours(0, 0, 0, 0);

  for (; n <= e;) {
    l[l.length] = n;
    n = addDays(n, 1);
  }

  return l;
}

/**
 * Calculate offset
 * @param {string} h
 * @param {number=} d - Duration in seconds
 * @return {string} Offset in hexadecimal
 */
function offset (h, d = 0) {
  return (convertBase36(h) + d).toString(36);
}
