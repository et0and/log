module.exports = class LogSet {
  /**
   * Construct set
   * @param {Array=} ent
   */
  constructor (ent = []) { this.logs = ent; }

  get count () { return this.logs.length; }

  get last () { return this.logs.slice(-1)[0]; }

  get lh () { return this.logHours(); }

  /**
   * Generate bar chart data
   * @param {Object=} config
   * @param {string=} config.cm - Colour mode
   * @param {string=} config.fg - Foreground colour
   * @return {Array} Data
   */
  bar ({cm, fg} = LOG.config) {
    if (this.count === 0) return [];
    const sort = this.sortEntries();
    let i = sort.length - 1;
    const data = [];

    if (cm === 'none') {
      for (; i >= 0; i--) {
        data[i] = [{
          height: `${new LogSet(sort[i]).coverage()}%`,
          backgroundColor: fg
        }];
      }
      return data;
    }

    for (; i >= 0; i--) {
      data[i] = [];
      for (let o = 0, day = sort[i], ol = day.length, lh = 0; o < ol; o++) {
        const x = day[o].wh;
        data[i][o] = {
          backgroundColor: day[o][cm] || fg,
          bottom: `${lh}%`,
          height: `${x}%`
        };
        lh += x;
      }
    }

    return data;
  }

  /**
   * Get logs by date
   * @param {Date=} d
   * @return {Array} Entries
   */
  byDate (d = new Date()) {
    const l = this.count;
    if (
      l === 0
      || typeof d !== 'object'
      || +d > +new Date()
    ) return [];

    const logs = [];

    function match (a) {
      return a.getFullYear() === d.getFullYear()
        && a.getMonth() === d.getMonth()
        && a.getDate() === d.getDate();
    }

    for (let i = 0; i < l; i++) {
      const {start, end} = this.logs[i];
      if (end !== undefined && match(start)) {
        logs[logs.length] = this.logs[i];
      }
    }

    return logs;
  }

  /**
   * Get logs by day
   * @param {number} d - Day of the week
   * @return {Array} Entries
   */
  byDay (d) {
    if (
      this.count === 0
      || typeof d !== 'number'
      || d < 0 || d > 6
    ) return [];
    function isValid ({start, end}) {
      return end !== undefined && start.getDay() === d;
    }
    return this.logs.filter(isValid);
  }

  /**
   * Get logs by month
   * @param {number} m - Month
   * @return {Array} Entries
   */
  byMonth (m) {
    if (
      this.count === 0
      || typeof m !== 'number'
      || m < 0 || m > 11
    ) return [];
    function isValid ({start, end}) {
      return end !== undefined && start.getMonth() === m;
    }
    return this.logs.filter(isValid);
  }

  /**
   * Get logs by period
   * @param {Date}  start
   * @param {Date=} end
   * @return {Array} Entries
   */
  byPeriod (start, end = new Date()) {
    if (
      this.count === 0
      || typeof start !== 'object'
      || typeof end !== 'object'
      || start > end
    ) return [];

    let logs = [];
    for (let now = start; now <= end;) {
      logs = logs.concat(this.byDate(now));
      now = addDays(now, 1);
    }

    return logs;
  }

  /**
   * Get logs by project
   * @param {string} term - Project
   * @param {Array=} list - Projects
   * @return {Array} Entries
   */
  byProject (term, list = LOG.cache.pro) {
    if (
      this.count === 0
      || typeof term !== 'string'
      || !Array.isArray(list)
      || list.indexOf(term) < 0
    ) return [];
    function isValid ({end, project}) {
      return end !== undefined && project === term;
    }
    return this.logs.filter(isValid);
  }

  /**
   * Get logs by sector
   * @param {string} term - Sector
   * @param {Array=} list - Sectors
   * @return {Array} Entries
   */
  bySector (term, list = LOG.cache.sec) {
    if (
      this.count === 0
      || typeof term !== 'string'
      || !Array.isArray(list)
      || list.indexOf(term) < 0
    ) return [];
    function isValid ({end, sector}) {
      return end !== undefined && sector === term;
    }
    return this.logs.filter(isValid);
  }

  /**
   * Calculate coverage
   * @return {number} Coverage
   */
  coverage () {
    const l = this.count;
    if (l === 0) return 0;

    const {end, start} = this.logs[0];
    const endd = l === 1 ? end : this.last.start;
    const dif = (endd - start) / 864E5;
    const n = (dif << 0) + 1;

    return (25 * this.lh) / (6 * n);
  }

  /**
   * Calculate average log hours per day
   * @return {number} Average log hours
   */
  dailyAvg () {
    const sort = this.sortEntries();
    const l = sort.length;
    return l === 0 ? 0
      : sort.reduce((s, c) => s + new LogSet(c).lh, 0) / l;
  }

  efficiency () {
    const l = this.count;
    if (l === 0) return 0;
    let score = 0;
    let count = 0;
    for (let i = 0; i < l; i++) {
      const x = this.logs[i].completionIndex;
      score += x ? x : 0;
      count++;
    }

    return score / this.count * 100
  }

  /**
   * Count entries per day
   * @return {Array} Entries per day
   */
  entryCounts () {
    if (this.count === 0) return 0;
    const sort = this.sortEntries();
    const l = sort.length;
    const counts = [];
    for (let i = 0; i < l; i++) {
      counts[counts.length] = sort[i].length;
    }
    return counts;
  }

  /**
   * List durations
   * @return {Array} List
   */
  listDurations () {
    const d = [];
    const l = this.count;
    if (l === 0) return d;
    const n = this.last.end === undefined ? 2 : 1;
    for (let i = l - n; i >= 0; i--) {
      d[d.length] = this.logs[i].dur;
    }
    return d;
  }

  /**
   * List focus
   * @param {number=} mode - Sector (0) or project (1)
   * @return {Array} List
   */
  listFocus (mode = 0) {
    const l = [];
    if (mode < 0 || mode > 1) return l;
    const sort = this.sortEntries();
    const sl = sort.length;
    if (sl === 0) return l;

    const key = `list${mode === 0 ? 'Sectors' : 'Projects'}`;

    for (let i = 0; i < sl; i++) {
      if (sort[i].length === 0) continue;
      l[l.length] = 1 / new LogSet(sort[i])[key]().length;
    }

    return l;
  }

  /**
   * List projects
   * @return {Array} List
   */
  listProjects () {
    const l = this.count;
    if (l === 0) return [];

    const n = this.last.end === undefined ? 2 : 1;
    const list = new Set();

    for (let i = l - n; i >= 0; i--) {
      list.add(this.logs[i].project);
    }

    return [...list];
  }

  /**
   * List sectors
   * @return {Array} List
   */
  listSectors () {
    const l = this.count;
    if (l === 0) return [];

    const n = this.last.end === undefined ? 2 : 1;
    const list = new Set();

    for (let i = l - n; i >= 0; i--) {
      list.add(this.logs[i].sector);
    }

    return [...list];
  }

  /**
   * Calculate logged hours
   * @return {number} Logged hours
   */
  logHours () {
    return this.count === 0 ? 0 : sum(this.listDurations());
  }

  /**
   * Calculate peak beats
   * @return {Array} Peaks
   */
  peakBeats () {
    const l = this.count;
    if (l === 0) return [];

    const n = this.last.end === undefined ? 2 : 1;
    const beats = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = l - n; i >= 0; i--) {
      const {start, dur} = this.logs[i];
      beats[~~(start.toDec() / 100)] += dur;
    }

    return beats;
  }

  /**
   * Get peak beat
   * @return {string} Peak beat
   */
  peakBeat () {
    const p = this.peakBeats();
    return p.length === 0 ? '-' : p.indexOf(max(p));
  }


  /**
   * Get peak day
   * @return {string} Peak day
   */
  peakDay () {
    const p = this.peakDays();
    return p.length === 0 ? '-' : Glossary.days[p.indexOf(max(p))];
  }

  /**
   * Calculate peak days
   * @return {Array} Peaks
   */
  peakDays () {
    const l = this.count;
    if (l === 0) return [];

    const n = this.last.end === undefined ? 2 : 1;
    const days = [0, 0, 0, 0, 0, 0, 0];

    for (let i = l - n; i >= 0; i--) {
      const {start, dur} = this.logs[i];
      days[start.getDay()] += dur;
    }

    return days;
  }

  /**
   * Get peak hour
   * @return {string} Peak hour
   */
  peakHour () {
    const p = this.peakHours();
    return p.length === 0 ? '-' : `${p.indexOf(max(p))}:00`;
  }

  /**
   * Calculate peak hours
   * @return {Array} Peaks
   */
  peakHours () {
    const l = this.count;
    if (l === 0) return [];

    const hours = Array(24).fill(0);

    for (let i = l - 1; i >= 0; i--) {
      const {start, end, dur} = this.logs[i];
      if (end === undefined) continue;
      let index = start.getHours();

      if (dur < 1) {
        hours[index] += dur;
        continue;
      }

      const rem = dur % 1;
      let block = dur - rem;

      hours[index]++;
      index++;
      block--;

      while (block > 1) {
        hours[index]++;
        index++;
        block--;
      }

      hours[index] += rem;
    }

    return hours;
  }

  /**
   * Get peak month
   * @return {string} Peak month
   */
  peakMonth () {
    const p = this.peakMonths();
    return p.length === 0 ? '-' : Glossary.months[p.indexOf(max(p))];
  }

  /**
   * Calculate peak months
   * @return {Array} Peaks
   */
  peakMonths () {
    const l = this.count;
    if (l === 0) return [];

    const n = this.last.end === undefined ? 2 : 1;
    const months = Array(12).fill(0);

    for (let i = l - n; i >= 0; i--) {
      const {start, dur} = this.logs[i];
      months[start.getMonth()] += dur;
    }

    return months;
  }

  /**
   * Calculate project counts
   * @return {Array} Counts
   */
  projectCounts () {
    if (this.count === 0) return [];
    const sort = this.sortEntries();
    const counts = [];

    for (let i = 0, l = sort.length; i < l; i++) {
      const set = new Set();
      for (let o = 0, ol = sort[i].length; o < ol; o++) {
        set.add(sort[i][o].project);
      }
      counts[counts.length] = [...set].length;
    }

    return counts;
  }

  /**
   * Get recent entries
   * @param {number} [n] - Number of days
   * @return {Array} Entries
   */
  recent (n = 1) {
    return n < 1 ? [] : this.byPeriod(addDays(new Date(), -n));
  }

  /**
   * Calculate sector counts
   * @return {Array} Counts
   */
  sectorCounts () {
    if (this.count === 0) return [];
    const sort = this.sortEntries();
    const counts = [];
    for (let i = 0, l = sort.length; i < l; i++) {
      const set = new Set();
      for (let o = 0; o < sort[i].length; o++) {
        set.add(sort[i][o].sector);
      }
      counts[counts.length] = [...set].length;
    }
    return counts;
  }

  /**
   * Sort entries by day
   * @return {Array} Sorted entries
   */
  sortByDay () {
    const l = this.count;
    if (l === 0) return [];
    const s = Array(7).fill([]);
    for (let i = l - 1; i >= 0; i--) {
      const d = this.logs[i].start.getDay();
      s[d][s[d].length] = this.logs[i];
    }
    return s;
  }

  /**
   * Sort entries
   * @param {Date=} end
   * @return {Array} Sorted entries
   */
  sortEntries (end = new Date()) {
    const el = this.count;
    if (el === 0) return [];

    const list = listDates(this.logs[0].start, end);
    const dates = {};

    for (let i = 0, l = list.length; i < l; i++) {
      dates[toDate(list[i])] = [];
    }

    for (let i = 0; i < el; i++) {
      const x = toDate(this.logs[i].start);
      if (x in dates) dates[x][dates[x].length] = this.logs[i];
    }

    return Object.keys(dates).map(i => dates[i]);
  }

  /**
   * TODO
   * Sort values
   * @param {number=} mode - Sector (0) | project (1)
   * @return {Array} Sorted values
   */
  sortValues (mode = 0) {
    if (
      this.count === 0
      || typeof mode !== 'number'
      || mode < 0 || mode > 1
    ) return [];

    const lhe = this.lh;
    const sort = [];
    const tmp = {};
    let list = [];
    let func = '';

    if (mode === 0) {
      list = this.listSectors();
      func = 'bySector';
    } else {
      list = this.listProjects();
      func = 'byProject';
    }

    for (let i = list.length - 1; i >= 0; i--) {
      const {lh} = new LogSet(this[func](list[i]));
      tmp[list[i]] = {p: lh / lhe * 100, h: lh};
    }

    const keys = Object.keys(tmp).sort((a, b) => tmp[a].h - tmp[b].h);
    for (let i = keys.length - 1; i >= 0; i--) {
      const {h, p} = tmp[keys[i]];
      sort[sort.length] = {h, p, n: keys[i]};
    }

    return sort;
  }

  /**
   * Calc streak
   * @return {number} Streak
   */
  streak () {
    if (this.count === 0) return 0;
    const sort = this.sortEntries();
    let s = 0;
    for (let i = 0, l = sort.length; i < l; i++) {
      s = sort[i].length === 0 ? 0 : s + 1;
    }
    return s;
  }
};
